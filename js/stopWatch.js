const hours = document.querySelector('#hour');
const minute = document.querySelector('#minute');
const second = document.querySelector('#second');
const milisecond = document.querySelector('#milisecond');
const btnStart = document.querySelector('#start-btn');
const btnReset = document.querySelector('#reset-btn');

let currentTime = 0;
let stop = true;

// let startTime = function() {

// }

// let appendZeroForMs = function(ms) {
//     if(ms <= 99 && ms > 9) {
//         return '0' + ms;
//     } else if(ms <= 9) {
//         return '00' + ms;
//     } else {
//         return ms;
//     }
// }

btnStart.addEventListener('click', function() {
    let timer = setInterval(function() {
        currentTime += 10;
        let appendMilisecond = parseInt((currentTime % 1000) / 10);
        let appendSecond = Math.floor((currentTime / 1000) % 60);
        let appendMinute = Math.floor((currentTime / (1000 * 60)) % 60);
        let appendHours = Math.floor((currentTime / (1000 * 60 * 60)) % 24);

        appendMilisecond = appendMilisecond < 10 ? '0' + appendMilisecond : appendMilisecond;
        milisecond.innerHTML = appendMilisecond;

        appendSecond = appendSecond < 10 ? '0' + appendSecond : appendSecond;
        second.innerHTML = appendSecond;

        appendMinute = appendMinute < 10 ? '0' + appendMinute : appendMinute;
        minute.innerHTML = appendMinute;

        appendHours = appendHours < 10 ? '0' + appendHours : appendHours;
        hours.innerHTML = appendHours;
    }, 10)
})


